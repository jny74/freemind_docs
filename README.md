# Freemind documentation #

This repository contains the source DITA XML files for "Freemind - Free your mind" guide, which is available at [add link here] and also available in [__alternative formats__] here [__add link here__].

### What is this repository for? ###

* All files have been validated against OASIS DITA 1.2 stock DTDs.
* Version

### How do I get set up? ###

* Download the latest the DITA OT
* Read the DITA OT documentation to configure your environment
* Dependencies

### Who do I talk to? ###

* Repo owner: Joakim Nybäck (joakim.nyback [at] gmail com)
* Other community or team contact