<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE task PUBLIC "-//OASIS//DTD DITA Task//EN" "task.dtd">
<task id="tsk_filtering">
	<title>Filtering</title>
	<shortdesc>A filter makes it possible to find text and icons in nodes matching multiple
		criteria, including Boolean operators.</shortdesc>
	<taskbody>
		<context>
			<p>A filter is an advanced query that is permantently stored for easy access. The nodes
				that match the query are shown including ancestors. Optionally descendants can be
				shown. You can continue editing the mindmap in filtered mode.</p>
		</context>
		<steps>
			<step>
				<cmd>Select the node "Exploration".</cmd>
			</step>
			<step>
				<cmd>Click the filter icon (<image keyref="filter_icon" outputclass="icon"
						placement="inline"/>).</cmd>
				<info>This displays the <uicontrol outputclass="toolbar">Filter</uicontrol> toolbar.<fig>
						<image href="graphics/tutorial_filter_toolbar.png" outputclass="toolbar"
							placement="break"/>
					</fig></info>
			</step>
			<step>
				<cmd>Click the dropdown menu and select <uicontrol>Currently Selected
						Nodes</uicontrol>.</cmd>
				<stepresult>Only the central node and the node "Exploration" are shown.</stepresult>
			</step>
			<step>
				<cmd>Select "Show Descendants".</cmd>
				<stepresult>All nodes in the branch "Exploration" are shown. <fig>
						<image href="graphics/tutorial_filtered_map.png" id="image_nk2_sv4_rp"
							outputclass="mindmap"/>
					</fig><note type="tip">When working with big mindmaps, this is a very handy
						feature for decluttering the screen and keeping a sharp focus on one
						branch.</note></stepresult>
			</step>
			<step>
				<cmd>Select <uicontrol>No filtering</uicontrol> from the dropdown menu.</cmd>
			</step>
			<step>
				<cmd>Click the edit icon (<image keyref="edit_filter_icon" outputclass="icon" placement="inline"
					/>) to launch the <wintitle>Filter Composer</wintitle>.</cmd>
				<info>
					<fig>
						<image href="graphics/tutorial_filter_composer_dialog.png"
							id="image_uhb_jw4_rp"/>
					</fig>
				</info>
			</step>
			<step>
				<cmd>Click the object dropdown menu and select <uicontrol>Icon</uicontrol>.</cmd>
				<stepresult>Freemind scans the mindmap and displays the icons used in the criteria
					dropdown.</stepresult>
			</step>
			<step>
				<cmd>In the criteria dropdown menu, select the surprised smiley (<image id="image_v1q_pms_qp" outputclass="icon"
					keyref="surprised_smiley_icon"/>).</cmd>
			</step>
			<step>
				<cmd>Click <uicontrol outputclass="button">Add</uicontrol>.</cmd>
				<stepresult>The filter you just defined is added in the <wintitle>Filters</wintitle> area and
					automatically selected. It will be available from <uicontrol>Quick
						filter</uicontrol> dropdown on the <uicontrol outputclass="toolbar"
						>Filter</uicontrol> toolbar<fig>
						<image href="graphics/tutorial_filter_composer_dialog_filter_applied.png"
							id="image_cmq_3cg_yp"/>
					</fig></stepresult>
			</step>
			<step>
				<cmd>Click <uicontrol outputclass="button">OK</uicontrol>.</cmd>
				<info>The <wintitle>Filter Composer</wintitle> dialog is closed. The mindmap now
					looks like this:<fig>
						<image href="graphics/tutorial_filtered_map_icon.png" outputclass="mindmap" id="image_klj_q1p_rp"
						/>
					</fig></info>
			</step>
			<step outputclass="extra_practice">
				<cmd>Create a filter showing nodes with the important icon (<image
						id="image_b2n_t1p_rp" keyref="important_icon" outputclass="icon"/>) and
					apply it.</cmd>
			</step>
			<step>
				<cmd>To remove the filter, click open the filter dropdown dialog and seclect <menucascade>
						<uicontrol>No Filtering</uicontrol>
					</menucascade>.</cmd>
				<info>
					<fig>
						<image href="graphics/tutorial_filter_dropdown_no_filtering.png"
							id="image_gpw_jcg_yp"/>
					</fig>
				</info>
			</step>
		</steps>
	</taskbody>
</task>
